<?php

use Code\Extend\Hook;

/**
 * Name: Fckmeta
 * Description: Block profile access to your site members from Meta Corp
 * Version: 1.0
 * Author: Macgirvin
 * Maintainer: none
 *
 */

function fckmeta_load() {
    Hook::register('channel_mod_init', 'addon/fckmeta/fckmeta.php', 'fckmeta_main');
    Hook::register('item_mod_init', 'addon/fckmeta/fckmeta.php', 'fckmeta_main');
    Hook::register('inbox_mod_init', 'addon/fckmeta/fckmeta.php', 'fckmeta_main');
    Hook::register('outbox_mod_init', 'addon/fckmeta/fckmeta.php', 'fckmeta_main');
    Hook::register('activity_mod_init', 'addon/fckmeta/fckmeta.php', 'fckmeta_main');
    Hook::register('conversation_mod_init', 'addon/fckmeta/fckmeta.php', 'fckmeta_main');
    Hook::register('followers_mod_init', 'addon/fckmeta/fckmeta.php', 'fckmeta_main');
    Hook::register('following_mod_init', 'addon/fckmeta/fckmeta.php', 'fckmeta_main');
    Hook::register('lists_mod_init', 'addon/fckmeta/fckmeta.php', 'fckmeta_main');
    Hook::register('profile_mod_init', 'addon/fckmeta/fckmeta.php', 'fckmeta_main');
    Hook::register('search_mod_init', 'addon/fckmeta/fckmeta.php', 'fckmeta_main');
}

function fckmeta_unload() {
    Hook::unregister_by_file('addon/fckmeta/fckmeta.php');
}

function fckmeta_useragents()
{
    return ['fb_iab', 'instagram', 'facebookexternalua'];
}

function fckmeta_main($args)
{
    foreach (fckmeta_useragents() as $agent) {
        if (str_contains(strtolower($_SERVER['HTTP_USER_AGENT']), $agent)) {
            http_status_exit(403, 'Permission denied.');
        }
    }
}
